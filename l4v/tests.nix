{ stdenv, lib, hostPlatform, fetchFromGitHub, writeScriptBin, buildPackages
, haskellPackages, python2Packages
, rsync, git, perl, hostname, which, cmake, ninja, dtc, libxml2
, isabelle, texlive-env, polyml, mlton, hol4
, initial-heaps
, source
, armv7Pkgs
, verbose ? false
, test_targets ? []
}:

let
  # cpp-wrapper = writeScriptBin "cpp" ''
  #  #!${buildPackages.runtimeShell}
  #  export L4CPP="-DTARGET=ARM -DTARGET_ARM -DPLATFORM=Sabre -DPLATFORM_Sabre"
  #  clang -Wno-invalid-pp-token -E -x c $@
  # '';

  ghcWithPackages = buildPackages.haskellPackages.ghcWithPackages (p: with p; [
    mtl base array containers transformers
    unix
  ]);

in
stdenv.mkDerivation {
  name = "sel4-tests";
  src = source;

  nativeBuildInputs = [
    python2Packages.sel4-deps
    texlive-env
    ghcWithPackages
    haskellPackages.cabal-install

    armv7Pkgs.stdenv.cc

    rsync
    git
    hostname
    which
    cmake
    ninja
    dtc
    libxml2

    # TODO
    (perl.nativeDrv or perl)

    mlton
    polyml
    hol4
  # ] ++ lib.optionals hostPlatform.isDarwin [
  #   # cpp-wrapper
  # ] ++ lib.optionals hostPlatform.isx86 [
  #   mlton

  ];

  # NIX_CFLAGS_COMPILE = [ "-Wno-unused-command-line-argument" ];
  # NIX_CFLAGS_LINK = [ "-Wno-unused-command-line-argument" "-lm" "-lffi" ];

  configurePhase = ''
    export L4V_ARCH=ARM_HYP
    export TOOLPREFIX=${armv7Pkgs.stdenv.cc.targetPrefix}
    export CROSS_COMPILER_PREFIX=${armv7Pkgs.stdenv.cc.targetPrefix}
    export SML_COMPILER=${if hostPlatform.isx86 then "mlton" else "poly"}
    export HOME=$NIX_BUILD_TOP/home

    cp -r ${initial-heaps} $HOME
    chmod -R +w $HOME
    mkdir -p $HOME/.cabal
    touch $HOME/.cabal/config

    cd l4v
  '';

  buildPhase = ''
    ./run_tests -j 1 --no-timeouts ${lib.optionalString verbose "-v"} ${lib.concatStringsSep " " test_targets}
  '';
    # ./run_tests -j $NIX_BUILD_CORES --no-timeouts ${lib.optionalString verbose "-v"} ${lib.concatStringsSep " " test_targets}

  installPhase = ''
    touch $out
  '';
}

# NOTE:
# RefineOrphanage depends on ./make_spec.sh having run
